# Dokument zahtev
 
| | |
|:---|:---|
| **Naziv projekta** | StraightAs |
| **Člani projektne skupine** | Andreja Jerič, Blaž Prosenc, Regina Polc, Katja Kušar |
| **Kraj in datum** | 7.4.2019 |
 
 
 
## Povzetek projekta
 
StraightAs je aplikacija, ki združuje funkcionalnosti koledarja, urnika, redovalnice in omogoča tudi ustvarjanje zapiskov. Namenjena je bolj učinkovitemu razporejanju časa med dejavnosti, s katerimi se ukvarjajo dijaki, študenti in profesorji. Njena posebnost je možnost, da vnešenim aktivnostim lahko določamo prioritete in na podalgi teh nam StriaghtAs lahko sam naredi optimalen načrt dela z določen dan ali teden – urnik dela. Poleg osnovnih funkcij kot so dodajanje dogodkov v koledar, predmetov v urnik in ocen v redovalnico, lahko dogodkom dodelimo tudi lokacijo in vse naštete stvari tudi uvozimo v aplikacijo ali izvozimo v datoteke. Prednost izvažanja in uvažanje je deljenje naših načrtov z drugimi ljudmi in posledično tudi lažje usklajevanje.

 
## 1. Uvod
 
Za projekt StraightAs smo se odločili, saj bi radi olajšali načrtovanje vsakodnevnih dejavnosti tako dijakov in študentov kot tudi profesorjev. Naša motivacija je bila lastna stiska z razporejanjem dela znotraj izobraževanja in prostočasnih dejavnosti, naš glavni cilj pa avtomatizirati in optimizirati načrtovanje dela skozi dneve.
Aplikacijo lahko uporabljajo registrirani in tudi neregistrirani uporabniki, slednji pa imajo omejene funkcionalnosti. Po registraciji se s kreiranim računom uporabnik lahko vpiše v sistem, svoj uporabniški račun in osebne podatke zapisane v njem, pa je možno urejati in spreminjati kadarkoli tekom uporabe aplikacije. Prav tako pa profesorji lahko zaprosijo za povišanje v status profesorja, le-to mora odobriti administrator.
Znotraj aplikacije pa so na voljo štiri glavne funkcionalnosti: urnik, koledar, redovalnica in beležka. Vsako od teh stvari lahko na novo ustvarimo kot prazen dokument, lahko pa v že obstoječih urejamo njene elemente. V redovalnico je možen vpis in urejanje ocen, podpira pa tudi možnost izvoza v tekstovno datoteko. V urnik si uporabniki lahko zapišejo predmete ali obveznosti, ki jih kasneje vedno lahko tudi urejajo in dodajajo še nove. Podobno velja za koledar, kamor si uporabnik lahko beleži dogodke, uro in čas le-tega ter tudi lokacijo, kjer so bo dogodek odvijal. Le-te prav tako lahko kasneje tudi ureja in dodaja nove. Poleg pregleda lastnega koledarja, lahko uporabnik dostopa tudi do koledarja drugih, ampak le do tistih, od katerih ima URL povezavo. Enako velja za dostope do urnikov drugih uporabnikov.
Elementom urnika in dogodkom v koledarju pa je možno nastaviti prioriteto in na podlagi te prioritete, nam aplikacija lahko avtomatsko generira načrt dela. Še druga lastnost dogodkov v koledarju pa je to, da jim lahko določimo tudi lokacijo.
Za hranjenje zapiskov je na voljo tudi beležka.
 
 
 
## 2. Uporabniške vloge
 
- **NEREGISTRIRANI UPORABNIK:**To je uporabnik, ki še ni opravil postopka registracije in se ne mor prijaviti v sistem. Lahko preglejuje urnike in koledarje preko URL naslova.
- **REGISTRIRANI UPORABNIK :** To je uporabnik, ki je opravil postopek registracije. Poleg funkcionalnosti, ki so omogočene neregistriranemu uporabniku, lahko ustvarja, preglejuje, deli in popravlja svoje koledarje, urnike, beležke in ocene.
- **VIŠJI REGISTRIRANI UPORABNIK:** To je uporabnik, ki je profesor.
- **ADMINISTRATOR**: Dodeljuje status višjega registriranega uporabnika registriranim uporabnikom, potem, ko preveri, da je registrirani uporabnik res profesor.
 
## 3. Slovar pojmov
 
- **Začetna stran:** To je spletna stran, ki se pokaže neregistriranim uporabnikom.
- **Domača stran:** To je spletna stran, ki se pokaže neregistriranim uporabnikom po uspešni prijavi.
- **Profesor:** Pedagoški delavec, ki uči v osnovni šoli, srednji šoli ali na fakulteti
- **Element:** To je obveznost, zabeležena na urniku. Periodično se pojavlja kot dogodek na urniku.
- **Dogodek:** To je obveznost, zabeležena na enem ali več koledarjih.
- **Uporabnik:** To je nadpomenka za neregistrirane, registrirane in višje registrirane uporabnike.
- **Mapa:** To je struktura, v katero lahko registrirani uporabnik ureja ustvarjene beležke.
- **Beležka:** To je tekstovni dokument, kamor uporabnik zabeleži želejeno besedilo.
- **Vsebina:** To je nadpomenka za dogodek, element in beležko.
- **Profil:** To je funkcionalnost, ki uporabniku omogoča ustvarjanje, hranjenje in pregled lastne vsebine.
- **Vloga:** To je forma, preko katere registrirani uporabnik odda prošnjo za dodelitev višjega statusa.
- **Višji status:** To je status, ki ga administrator lahko dodeli registriranim uporabnikom, ki zaprosijo zanj in podajo dokazilo, da so profesorji.
- **Urnik dela:** To je urnik, ki poleg rednih elementov prikaže tudi dogodke ter vse skupaj razporedi po prioriteti. Prav tako omogoča označevanje že opravljenih elementov in dogodkov.
- **Vidljivost:** To je lastnost vsebine, ki določa, če ji je dodeljena URL povezava, preko katere si ga lahko ogledajo vsi uporabniki
- **Izvoz:** To je postopek, s katerim uporabnik izbrano vsebino shrani v datoteko, ki se shrani na napravo, s katero dostopa do sistema.
- **Osebni podatki:** To so ime, priimek, letnik šolanja, smer šolanja in šola, na kateri se uporabnik izobražuje ali uči.
- **Uporabniško ime:** To je unikaten niz, ki si ga uporabnik izbere, ko si ustvari profil in ga nato uporabi za vpis na stran v kombinaciji s svojim geslom.
- **Prijava:** Sinonim za vpis. 
 
 
## 4. Diagram primerov uporabe
 
![grafPrimeraUporabe](../img/graf.png)
 
 
## 5. Funkcionalne zahteve

###**REGISTRACIJA**
 
#### Povzetek funkcionalnosti
Neregistrirani uporabnik se lahko registrira. Pri tem mora podati veljaven elektronski naslov in uporabniško ime, ki še nista bila uporabljena, poljubno geslo in osebne podatke. Neregistrirani uporabnik mora registracijo potrditi preko potrditvene povezave, ki ga prejme na elektronski naslov, ki ga je podal med registracijo.
 
#### Osnovni tok:
1.  	Neregistriran uporabnik odpre začetno stran.
2.  	Na začetni strani klikne na gumb prijava/registracija.
3.  	Odpre se meni za prijavo in registracijo.
4.  	Odpre zavihek za registracijo.
5.  	Polja zapolni z ustreznimi podatki.
6.  	Klikne na potrditev registracije.
7.  	Na elektronski naslov, ki ga je podal, se pošlje potrditveno povezavo.
8.  	Odpre link in registracija se zaključi.
9.  	Odpre se mu domača stran.
 
 
#### Izjemni tokovi:
-          Neregistrirani uporabnik ne vnese vseh zahtevanih podatkov. Sistem prikaže ustrezno obvestilo.
-          Neregistrirani uporabnik vpiše neveljavne podatke. Sistem izpiše obvestilo.
-          Neregistrirani uporabnik izbere uporabniško ime, ki je že v uporabi. Sistem prikaže ustrezno obvestilo.
-          Neregistrirani uporabnik se poskusi registrirati z elektronskim naslovom, ki je že bil registriran. Sistem prikaže ustrezno obvestilo.
-          Neregistrirani uporabnik izbere prekratko geslo. Sistem prikaže ustrezno obvestilo.
 
#### Pogoji
Uporabnik mora imeti internetno povezavo za dostop do spletne strani. Uporabnik ni prijavljen v sistem.
 
#### Posledice
Dobimo novega registriranega uporabnika - neregistrirani uporabnik postane registrirani uporabnik. Njegov elektronski naslov in uporabniško ime so registrirani.
 
#### Prioritete identificiranih funkcionalnosti
**Must have.** Vsak uporabnik mora narediti profil, če hoče uporabljati večino funkcionalnosti našega sistema, in da se obdržijo vse spremembe (datumi izpitov, beležka,...).
 
#### Sprejemni testi
-          Uporabnik odpre meni za registracijo, vpiše pravilne podatke, potrdi registracijo in aktivira profil preko vnešenega elektronskega naslova.
-          Uporabnik se želi registrirati z elektronskim naslovom, ki je že bil registriran.
-          Uporabnik odpre meni za registracijo, pri vpisu podatkov pozabi vpisati uporabniško ime.
-          Uporabnik odpre meni za registracijo, pri vpisu podatkov v polje za elektronski naslov vpiše elektronski naslov brez znaka @.
-          Uporabnik odpre meni za registracijo, pri vpisu podatkov v polje za uporabniško ime vpiše uporabniško ime, ki je že v uporabi.
-          Uporabnik odpre meni za registracijo, pri vpisu podatkov v polje za geslo vpiše samo en znak.

###**PRIJAVA**
 
#### Povzetek funkcionalnosti
Registrirani uporabnik se lahko prijavi. Pri tem mora vnesti elektronski naslov ali uporabniško ime in geslo, ki ju je vpisal med registracijo.
 
#### Osnovni tok:
1.  	Registriran uporabnik odpre začetno stran.
2.  	Klikne na gumb za registracijo/vpis.
3.  	Odpre se okno za vpis.
4.  	Izpolni uporabniško ime in geslo.
5.  	Pritisne gumb za potrditev.
6.  	Vpis uspe in preusmeri ga na domačo stran, kjer je vpisan.
 
#### Alternativni tok:
1.  	Registriran uporabnik odpre začetno stran.
2.  	Klikne na gumb za registracijo/vpis.
3.  	Odpre se okno za vpis.
4.  	Izpolni elektronski naslov in geslo.
5.  	Pritisne gumb za potrditev.
6.  	Vpis uspe in preusmeri ga na domačo stran, kjer je vpisan.
 
#### Izjemni tokovi:
-          Uporabnik vnese napačno kombinacijo uporabniškega imena in gesla. Sistem izpiše ustrezno obvestilo.
-          Uporabnik vnese napačno kombinacijo elektronskega naslova in gesla. Sistem izpiše ustrezno obvestilo.
-          Uporabnik vpiše uporabniško ime ali elektronski naslov, ki še ni registriran. Sistem izpiše ustrezno obvestilo.
 
#### Pogoji
Uporabnik je moral enkrat pred prijavo opraviti postopek registracije. Uporabnik ni prijavljen v sistemu.
 
#### Posledice
Registrirani uporabnik je vpisan v sistem.
 
#### Prioritete identificiranih funkcionalnosti
**Must have.** Registracija je nesmiselna, če se uporabnik ne more prijaviti.
 
#### Sprejemni testi
-          Uporabnik na začetni strani vpiše svoje uporabniško ime in geslo ter se prijavi.
-          Uporabnik na začetni strani vpiše svoj elektronski naslov in geslo ter se prijavi.
-          Uporabnik na začetni strani vpiše svoj elektronski naslov in napačno geslo.
-          Uporabnik na začetni strani vpiše svoje uporabniško ime in napačno geslo.
-          Uporabnik in začetni strani vpiše neregistrirano uporabniško ime.

###**ODJAVA**
 
#### Povzetek funkcionalnosti
Registrirani uporabnik se lahko odjavi iz sistema.
 
#### Osnovni tok:
1.  	Registriran uporabnik klikne na gumb za odjavo.
2.  	Sistem uporabnika odjavi in preusmeri na začetno stran.
 
#### Pogoji
Registrirani uporabnik mora biti prijavljen v sistemu.
 
#### Posledice
Registrirani uporabnik ni več prijavljen v sistemu.
 
#### Prioritete identificiranih funkcionalnosti
**Must have.** Uporabnik se odjavi, da zaščiti profil (če bi bil stalno prijavljen na računalniku, bi lahko kdo spremenil podatke v profilu) in da možnost, da se na istem računalniku lahko kasneje nekdo drug prijavi.
 
#### Sprejemni testi
-          Uporabnik klikne na gumb odjava.
 

###**UREJANJE OSEBNIH PODATKOV, UPORABNIŠKEGA IMENA, GESLA IN SLIKE PROFILA**
 
#### Povzetek funkcionalnosti
Registrirani uporabnik lahko ureja osebne podatke, uporabniško ime, geslo in sliko profila.
 
#### Osnovni tok:
1.  	Uporabnik je prijavljen na strani.
2.  	Klikne na ikono profila.
3.  	Izbere opcijo za urejanje podatkov.
4.  	Spreminja podatke v poljih.
5.  	Ko je končal, klikne gumb za shranitev.
6.  	Podatki se shranijo.
7.        Uporabnik je preusmerjen na domačo stran. 
 
#### Izjemni tokovi:
-          Uporabnik želi spremeniti svoje uporabniško ime z uporabniškim imenom, ki je že v uporabi. Sistem izpiše ustrezno obvestilo.
-          Uporabnik želi spremeniti svoje geslo in izbere prekratko geslo. Sistem izpiše ustrezno obvestilo.
-          Uporabnik želi spremeniti svoje geslo in pri potrditvi gesla vnese drugačno geslo. Sistem izpiše ustrezno obvestilo.
 
#### Pogoji
Uporabnik mora biti prijavljen v sistem.
 
#### Posledice
Osebni podatki, uporabniško ime, geslo ali slika profila so spremenjeni. Staro uporabniško ime ni več registrirano in ga lahko neregistrirani uporabniki uporabijo pri registraciji. Novo uporabniško ime je registrirano in ga neregistrirani uporabniki ne morejo uporabiti pri registraciji.
 
#### Prioritete identificiranih funkcionalnosti
**Should have.** Če se ob registraciji uporabnik zatipka, lahko to kasneje popravi. Uporabniku se lahko po registraciji spremenijo podatki (na primer izobraževalna ustanova, priimek), zato je za dobro uporabniško izkušnjo dobro, da ima opcijo spremeniti osebne podatke.
 
#### Sprejemni testi
-          Uporabnik spremeni svoje geslo in za novo geslo izbere besedno zvezo dolgo 6 znakov, ki ne vsebuje presledka.
-          Uporabnik spremeni svoje uporabniško ime v uporabniško ime, ki že obstaja.
-          Uporabnik spremeni svoje geslo in za novo geslo izbere besedno zvezo dolgo 3 znake.
-          Uporabnik spremeni svoje geslo in za novo geslo izbere besedno zvezo dolgo 6 znakov, ki ne vsebuje presledka, pri potrditvi gesla pa vpiše drugačno geslo.


###**ODDAJA VLOGE ZA POVIŠANJE STATUSA V VIŠJI REGISTRIRANI UPORABNIK**
 
#### Povzetek funkcionalnosti
Registrirani uporabnik lahko odda vlogo za povišanje statusa v višji registrirani uporabnik. Pri tem mora vnesti naziv ustanove, na kateri uči.
 
#### Osnovni tok: 
1.  	Uporabnik je vpisan v sistem.
2.  	Klikne na ikono profila v zgornjem desnem kotu.
3.  	Izbere opcijo za spremembo statusa.
4.  	Odpre se pogovorno okno.
5.  	Izpolni podatke o izobraževalni ustanovi, kjer uči.
6.  	Klikne gumb »Pošlji«.
 
#### Izjemni tokovi:
-          Uporabnik ne vnese vseh zahtevanih podatkov. Sistem izpiše ustrezno obvestilo.
 
#### Pogoji
Uporabnik je prijavljen v sistem in nima višjega statusa.
 
#### Posledice
Registrirani uporabnik postane kandidat za povišanje statusa. Registrirani uporabnik lahko postane višji registrirani uporabnik. Administrator prejme vlogo registriranega uporabnika.
 
#### Prioritete identificiranih funkcionalnosti
**Should have.**
 
#### Sprejemni testi
-          Uporabnik odda vlogo za spremembo statusa in izpolni vsa zahtevana polja.
-          Uporabnik oddaja vlogo za spremembo statusa in ne navede naziva izobraževalne ustanove.
 

###**ZAKLJUČEVANJE VLOG ZA POVIŠANJE STATUSA**
 
#### Povzetek funkcionalnosti
Administrator lahko zaključi vloge za povišanje statusa, ki so ga oddali registrirani uporabniki. Pri tem mora podatke registriranega uporabnika, ki je oddal vlogo, overiti pri izobraževalnem zavodu.
 
#### Osnovni tok: 
1.  	Administrator je vpisan v sistem.
2.  	Na domači strani klikne »nabiralnik« za prejete vloge.
3.	    Odpre se mu seznam vlog.
4.  	Klikne na vlogo.
5.	    Overi podatke pri izobraževalnem zavodu.
6.	    Zavod sporoči, da so podatki resnični
7.  	Administrator klikne na gumb za potrditev.
8.  	Izpiše se napis »Potrditev uspešna«.
9.  	Preusmeri ga nazaj v nabiralnik.

 
#### Alternativni tok:
1.  	Administrator je vpisan v sistem.
2.  	Na domači strani klikne »nabiralnik« za prejete vloge.
3.  	Odpre se mu seznam vlog.
4.  	Klikne na vlogo.
5.	    Overi podatke pri izobraževalnem zavodu.
6.	    Zavod sporoči, da podatki niso resnični ali točni
7.  	Klikne na gumb za zavrnitev.
8.  	Izpiše se napis »Potrditev zavrnjena«.
9.  	Preusmeri ga nazaj v nabiralnik.
 
#### Pogoji
Administrator mora biti prijavljen v sistem in imeti nezaključene vloge v nabiralniku.
 
#### Posledice
Registriranemu uporabniku se vloga potrdi - postane višji registrirani uporabnik - ali zavrne - ostane registrirani uporabnik. Vloga se izbriše.
 
#### Prioritete identificiranih funkcionalnosti
** Should have.**
 
#### Sprejemni testi
-          Admin preveri pravilnost vloge in jo potrdi.
-          Admin preveri pravilnost vloge in jo zavrne.
 

###**IZVOZ OCEN**
 
#### Povzetek funkcionalnosti
Registrirani uporabnik lahko izvozi ocene. Pri tem lahko izbere poljubne ocene.
 
#### Osnovni tok:
1.  	Uporabnik je vpisan v sistem.
2.  	Na domači strani klikne na redovalnico.
3.  	Izbere opcijo za izvoz.
4.  	Odpre se pogovorno okno.
5.  	Izbere, katere ocene želi izvoziti.
6.  	Klikne na gumb za potrditev.
7.  	Podatki se izvozijo.
8.  	Uporabnik je preusmerjen nazaj na redovalnico.
 
#### Izjemni tokovi:
-          Uporabnik želi izvoziti ocene, a ne izbere nobene ocene. Sistem izpiše ustrezno obvestilo.
-          Uporabnik želi izvoziti ocene, a nima vnešene nobene ocene. Sistem prikaže ustrezno obvestilo.
 
#### Pogoji
Uporabnik mora biti prijavljen v sistem in mora imeti ustvarjeno vsaj eno oceno.
 
#### Posledice
Registrirani uporabnik ustvari datoteko z izbranimi ocenami.
 
#### Prioritete identificiranih funkcionalnosti
**Could have.**
 
#### Sprejemni testi
-          Uporabnik izbere ocene in jih izvozi.
-          Uporabnik ne izbere nobene ocene in jih izvozi.
-          Uporabnik nima nobene ocene in jih izvozi.
 

###**PREGLED URNIKA PREKO URL POVEZAVE**
 
#### Povzetek funkcionalnosti

Neregistrirani in registrirani uporabniki lahko pregledujejo urnike registriranih uporabnikov.

#### Osnovni tok 1:
1.  	Registrirani uporabnik se vpiše v sistem.
2.      Na domači strani v search bar prilepi URL urnika.
3.  	Pritisne gumb za iskanje.
4.  	Izpiše se mu urnik.
5.  	Lahko izbira med različnimi pogledi (izbira samo določenih elementov, itd).

###Alternativni tok 1:
1.	    Uporabnik ni prijavljen v sistemu
2.      Na domači strani v search bar prilepi URL urnika.
3.  	Pritisne gumb za iskanje.
4.  	Izpiše se mu urnik.
5.  	Lahko izbira med različnimi pogledi (izbira samo določenih elementov, itd).
 
#### Izjemni tokovi:
-          Uporabnik vpiše URL, ki ne pripada nobenemu urniku.
 
#### Pogoji
URL povezava je veljavna.
 
#### Posledice
Pregled urnika.
 
#### Prioritete identificiranih funkcionalnosti
**Could have.**
 
#### Sprejemni testi
-          Neregistrirani uporabnik na začetni strani vnese URL povezavo urnika.
-          Registrirani uporabnik se prijavi in na domači strani vnese URL povezavo urnika.
-          Uporabnik vnese neveljaven URL.


###**PREGLED  LASTNEGA URNIKA**
 
#### Povzetek funkcionalnosti

Registrirani uporabnik lahko pregleduje svoj urnik.

#### Osnovni tok:
1.  	Uporabnik se vpiše v sistem.
2.  	Na domači strani izbere urnik, ki ga želi pregledati.
3.	    Odpre se urnik.
 
#### Pogoji
Registrirani uporabnik je prijavljen v sistem in ima ustvarjen vsaj en lasten urnik.
 
#### Posledice
Pregled urnika.
 
#### Prioritete identificiranih funkcionalnosti
**Should have.**
 
#### Sprejemni testi
-          Uporabnik se prijavi, klikne na svoj urnik.

###**PREGLED KOLEDARJA PREKO URL POVEZAVE**
 
#### Povzetek funkcionalnosti
Neregistrirani in registrirani uporabnikI lahko pregledujejo koledarje registriranih uporabnikov.
 
#### Osnovni tok 1:
1.      Uporabnik se prijavi v sistem.
2.        Na domači strani v search bar prilepi URL koledarja.
3.      Pritisne gumb za iskanje.
4.      Izpiše se mu koledar.
5.      Lahko izbira med različnimi pogledi (izbira samo določenih elementov, itd).

###Alternativni tok 1:
1.    Uporabnik ni prijavljen v sistemu
2.        Na domači strani v search bar prilepi URL koledarja.
3.      Pritisne gumb za iskanje.
4.      Izpiše se mu koledar.
5.      Lahko izbira med različnimi pogledi (izbira samo določenih elementov, itd).
 
#### Izjemni tokovi:
-          Uporabnik vpiše URL, ki ne pripada nobenemu koledarju. Sistem pobriše search bar in izpiše ustrezno obvestilo.
 
#### Pogoji
URL povezava je veljavna.
 
#### Posledice
Pregled koledarja.
 
#### Prioritete identificiranih funkcionalnosti
Pregled koledarja z URL povezavo: **Could have.**
 
#### Sprejemni test
-          Uporabnik na začetni strani vnese URL povezavo koledarja.
-      Uporabnik na domači strani vnese URL povezavo koledarja.
-          Uporabnik na začetni strani vnese neveljaven URL.
 
###**PREGLED LASTNEGA KOLEDARJA**
 
#### Povzetek funkcionalnosti
Registrirani uporabniki lahko pregledujejo svoj koledar.
 
#### Osnovni tok 1:
1.      Uporabnik se prijavi v sistem.
2.      Na domači strani izbere koledar, ki ga želi pregledati.
3.    Odpre se koledar.
5.      Lahko izbira med različnimi pogledi (izbira samo določenih elementov, itd).
 
#### Pogoji
Pregled lastnega urnika: registrirani uporabnik je prijavljen v sistem in ima ustvarjen vsaj en koledar
 
#### Posledice
Pregled koledarja.
 
#### Prioritete identificiranih funkcionalnosti
**Must have.**
 
#### Sprejemni test
-          Uporabnik se prijavi, klikne na svoj koledar.

 
###**USTVARJANJE BELEŽK**
 
#### Povzetek funkcionalnosti
Registrirani uporabnik lahko ustvari nove beležke.
 
#### Osnovni tok 1:
1.      Uporabnik je vpisan v sistem.
2.      Na domači strani izbere gumb dodaj in izbere dodajanje beležke.
3.    Odpre se pogovorno okno.
4.      Izpolni naslov in mesto, kjer bo shranjena.
5.      Klikne na gumb za shranitev.
6.      Beležka se shrani.
7.      Uporabnik je preusmerjen in se mu odpre pravkar ustvarjena beležka, ki jo lahko ureja. 
 
#### Izjemni tokovi:
-          Uporabnik poimenuje beležko z imenom, ki pripada že neki drugi beležki. Sistem izpiše ustrezno obvestilo.
-          Uporabnik beležki ne izbere imena. Sistem izpiše ustrezno obvestilo.

#### Pogoji
Uporabnik mora biti prijavljen v sistem.
 
#### Posledice
Uporabnik ima ustvarjeno novo beležko. Ime beležke je zasedeno - uporabnik ne more narediti nove beležke s tem imenom.
 
#### Prioritete identificiranih funkcionalnosti
**Could have.** Registriran uporabnik ima na voljo beležko, kjer si lahko zapiše dodatne podatke.
 
#### Sprejemni testi
-          Uporabnik doda beležko in jo poimenuje.
-          Uporabnik poimenuje novo beležko z imenom, ki že pripada neki drugi njegovi beležki.
-          Uporabnik doda beležko in ji ne izbere imena.
 

###**UREJANJE BELEŽK**
 
#### Povzetek funkcionalnosti

Registrirani uporabnik lahko ureja že ustvarjene beležke.

#### Osnovni tok:
1.      Uporabnik je vpisan v sistem.
2.      Izbere beležko, ki jo hoče urediti
3.    Odpre se pogovorno okno.
4.    Uporabnik piše in ureja besedilo v beležki in spreminja ime beležke.
5.    Klikne na gumb za shranitev
6.    Beležka je shranjena

#### Pogoji
Uporabnik mora biti prijavljen v sistem in mora imeti ustvarjeno vsaj eno beležko.
 
#### Posledice
Spremenjena vsebina beležke, ki je že bila ustvarjena. Le je uporabnik spremenil ime beležke, je novo ime beležke zasedeno in ga uporabnik ne more uporabiti za druge beležke. Staro ime beležke pa se sprosti.
 
#### Prioritete identificiranih funkcionalnosti
**Could have.** Registriran uporabnik lahko spreminja že ustvarjene beležke.
 
#### Sprejemni testi
-      Uporabnik izbere beležko in ji spremeni vsebino
 

###**AVTOMATSKO GENERIRANJE NAČRTA DELA**
 
#### Povzetek funkcionalnosti
Registrirani uporabnik lahko izbere funkcijo za avtomatsko generiranje načrta dela.

#### Osnovni tok:
1.      Uporabnik je vpisan v sistem.
2.      Na domači strani izbere urnik.
3.      Izbere opcijo za kreiranje načrta.
4.      Odpre se pogovorno okno za izbiro elementov in dogodkov, ki morajo biti del načrta.
5.      Izbere elemente.
6.      Klikne gumb za shranitev.
7.      Naredi se načrt.
8.      Uporabniku se odpre načrt.
 
#### Izjemni tokovi:
-       Uporabnik želi ustvariti načrt, a ne izbere nobenega elementa. Sistem izpiše ustrezno obvestilo.
-       Uporabnik želi ustvariti načrt, a še nima ustvarjenih elementov ali dogodkov. Sistem izpiše ustrezno obvestilo.
 
#### Pogoji
Uporabnik mora biti prijavljen v sistem in imeti ustvarjen vsaj en dogodek ali element.
 
#### Posledice
Generiran je načrt dela.
 
#### Prioritete identificiranih funkcionalnosti
**Could have.**
 
#### Sprejemni testi
-          Uporabnik izbere generiranje načrta in izbere elemente in dogodke, za katere želi, da so del načrta.
-          Uporabnik izbere generiranje načrta in ne izbere nobenega elementa in dogodka.
-          Uporabnik izbere generiranje načrta, a še nima ustvarjenega nobenega elementa in dogodka.

###**LOCIRANJE DOGODKOV PREKO ZEMLJEVIDA**
 
#### Povzetek funkcionalnosti
Registrirani uporabnik lahko locira dogodke preko zemljevida.
 
#### Osnovni tok:
1.       Uporabnik je vpisan v sistem.
2.       V izbranem koledarju izbere dogodek.
3.       Odpre se pogovorno okno urejanje.
4.       Izbere opcijo za dodajanje na zemljevid.
5.       Odpre se zemljevid.
6.       Uporabnik na zemljevidu klikne lokacijo.
7.       Uporabnik klikne gumb za shranitev.
8.       Shrani se lokacija.
9.       Uporabnik je preusmerjen na koledar.
 
#### Pogoji
Uporabnik mora biti prijavljen v sistem in mora imeti ustvarjen vsaj en dogodek.
 
#### Posledice
Dogodek ima določeno lokacijo.
 
#### Posebnosti
Brskalnik mora omogočiti uporabo lokacije.
 
#### Prioritete identificiranih funkcionalnosti
**Won’t have.**

#### Sprejemni testi
Uporabnik izbere dogodek in mu dodeli lokacijo.

 
 
 

###**USTVARJANJE URNIKA**
 
#### Povzetek funkcionalnosti

Registrirani uporabnik lahko ustvari nov urnik. Pri tem mu mora določiti ime in vidljivost.

#### Osnovni tok
1.  	Uporabnik je vpisan v sistem.
2.  	Na domači strani klikne na dodajanje urnika.
3.	Odpre se pogovorno okno, kjer izbere dodajanje urnika.
4.  	Urnik poimenuje in mu določi vidljivost.
5.  	Pritisne na gumb za potrditev.
6.  	Ustvari se prazen urnik.
7.  	Uporabnika preusmeri na novo ustvarjeni urnik.
 
#### Izjemni tokovi:
-          Uporabnik ustvari urnik in mu izbere ime, ki je enako enemu njegovih že obstoječih urnikov. Sistem izpiše ustrezno obvestilo.
-          Uporabnik pri ustvarjanju urnika ne nastavi vidljivosti. Sistem izpiše ustrezno obvestilo.
 
#### Pogoji
Uporabnik mora biti prijavljen v sistem.
 
#### Posledice
Uporabnik ima ustvarjen nov urnik. Ime urnika postane zasedeno, uporabnik ga ne more uporabiti pri kreiranju novih urnikov.
  
#### Prioritete identificiranih funkcionalnosti
**Should have.**
 
#### Sprejemni testi
-          Uporabnik doda urnik, mu izbere ime, določi vidljivost in klikne gumb za potrditev.
-      Uporabnik doda urnik, ki mu izbere ime, ki ga je že določil obstoječemu urniku.
-      Uporabnik doda urnik, ki mu ne nastavi vidljivosti. 
 
###**UREJANJE ELEMENTOV V URNIKU**
 
#### Povzetek funkcionalnosti
Registrirani uporabnik lahko ureja že obstoječe elemente v urniku.

#### Osnovni tok:
1. Uporabnik je prijavljen na strani. 
2. Uporabnik izbere urnik. 
3. Urnik se odpre. 
4. Uporabnik izbere element, ki ga želi urediti in klikne nanj. 
5. Odpre se pogovorno okno, kjer lahko spremeni ime, čas, dan in oznake elementa. 
6. Pritisne na gumb za shranitev. 
7. Uporabnika preusmeri nazaj na urnik. 

#### Izjemni tokovi
Uporabnik ureja element in ga želi poimenovati z imenom, ki ga že ima nek drug element. Sistem izpiše ustrezno obvestilo. 
 
#### Pogoji
Uporabnik mora biti prijavljen v sistem in mora imeti ustvarjen urnik z vsaj enim elementom.
 
#### Posledice
Spremenjeni podatki elementov v urniku. Če je uporabnik spremenil ime elementa, postane zasedeno in ga uporabnik ne more dodelit drugim elementom, staro ime elementa se sprosti.
  
#### Prioritete identificiranih funkcionalnosti
**Should have.**
 
#### Sprejemni testi
-      Uporabnik izbere urnik, in enemu od elementov spremeni na ime, ki ne pripada nobenemu drugemu elementu. 
-          Uporabnik elementu dodeli novo ime, ki že pripada nekemu drugemu elementu.
 

###**DODAJANJE ELEMENTOV V URNIKU**
 
#### Povzetek funkcionalnosti
Registrirani uporabnik lahko dodaja elemente v urnik.
 
#### Osnovni tok: Registrirani uporabnik lahko dodaja elemente v obstoječ urnik.
1.  	Uporabnik je prijavljen na strani.
2.  	Uporabnik izbere urnik.
3.  	Urnik se odpre.
4.  	Uporabnik v izbranem urniku izbere ikono za dodajanje. 
5.  	Uporabnik v pogovornem oknu izbere ime, oznake, dan in uro elementa.
6.  	Pritisne na gumb za shranitev.
7.  	Spremembe se shranijo.
8.  	Uporabnika preusmeri nazaj na urnik.

#### Izjemni tokovi
Uporabnik ustvari element in mu izbere ime, ki je enako enemu njegovih že obstoječih elementov. Sistem izpiše ustrezno obvestilo.
 
#### Pogoji
Uporabnik mora biti prijavljen v sistem in mora imeti ustvarjen vsaj en urnik.
 
#### Posledice
Dodani podatki v urniku. Ime elementa postane zasedeno.
  
#### Prioritete identificiranih funkcionalnosti
**Should have.**
 
#### Sprejemni testi
-          Uporabnik izbere urnik, izbere urejanje in doda element, ki mu določi ime, dan, uro in oznake.
-          Uporabnik ustvari element z imenom, ki je že dodeljeno nekemu drugemu element.
 
 
###**USTVARJANJE KOLEDARJA**
 
#### Povzetek funkcionalnosti
Registrirani uporabnik lahko ustvari koledar. Pri tem mu mora določiti ime in vidljivost.
 
#### Osnovni tok:
1.       Uporabnik je vpisan v sistem.
2.       Na domači strani klikne gumb za dodajanje.
3.       Izbere opcijo za dodajanje koledarja.
4.       Odpre se pogovorno okno.
5.       Izpolni polja za ime koledarja in vidljivost.
6.       Nato klikne na gumb za shranitev.
7.       Uporabnik je preusmerjen na novo ustvarjen koledar. 
 
#### Izjemni tokovi:
-          Uporabnik želi ustvariti koledar z imenom, ki pripada že obstoječemu koledarju. Sistem izpiše ustrezno obvestilo.
-          Uporabnik ne določi vidljivosti koledarja. Sistem izpiše ustrezno obvestilo.
 
#### Pogoji
Uporabnik mora biti prijavljen v sistem.
 
#### Posledice
Uporabnik ima ustvarjen nov koledar. Ime koledarja postane zasedeno.
 
#### Prioritete identificiranih funkcionalnosti
**Must have.** Da lahko registriran uporabnik bolje sledi svojim raznolikim obveznostim, mora imeti možnost ustvarjanja enega ali več koledarjev (npr. koledar za šolo, koledar za obšolske dejavnosti).
 
#### Sprejemni test
-          Uporabnik doda koledar, ki mu izbere ime in vidljivost.
-          Uporabnik doda koledar, ki mu izbere ime, ki že pripada nekemu drugemu koledarju.
-          Uporabnik doda koledar, ki mu ne določi vidljivosti.
 
 
###**DODAJANJE DOGODKOV V KOLEDARJU**
 
#### Povzetek funkcionalnosti
Registrirani uporabnik lahko ustvari dogodke v koledarju. 
 
#### Osnovni tok:
1.  	Uporabnik je vpisan v sistem.
2.  	Na domači strani izbere koledar, kamor želi dodati dogodek..
3.  	Izbere opcijo za dodajanje dogodka.
4.  	Odpre se pogovorno okno.
5.  	Izpolni polja za ime dogodka, datum in pomembnost.
6.  	Nato klikne na gumb za shranitev.
7.  	Dogodek se shrani.
8.  	Uporabnik je preusmerjen nazaj na koledar. 
 
#### Izjemni tokovi:
-          Uporabnik doda nov dogodek, ki ima isto ime, datum in prioriteto in oznake kot že nek obstoječ dogodek. Sistem izpiše ustrezno obvestilo.

 
#### Pogoji
Uporabnik mora biti prijavljen v sistem.
 
#### Posledice
Dodan nov dogodek.
 
#### Prioritete identificiranih funkcionalnosti
**Must have.** Da so uporabniku ustvarjeni koledarji koristni, mora imeti možnost dodajanja dogodkov vanje. Ime dogodka postane zasedeno.
 
 
#### Sprejemni testi
-          Uporabnik izbere koledar, izbere dodajanje dogodka, kjer določi ime, datum in pomembnost.
-          Uporabnik doda dogodek, ki ima isto ime, datum in pomembnost kot že obstoječ dogodek.
 

###**UREJANJE DOGODKOV V KOLEDARJU**
 
#### Povzetek funkcionalnosti
Registrirani uporabnik lahko ureja vnose v koledarju.
 
#### Osnovni tok:
1.  	Uporabnik je vpisan v sistem.
2.  	Na domači strani izbere koledar, na katerem je dogodek, ki ga želi urediti.
3.  	Izbere dogodek.
4.  	Izbere opcijo za urejanje dogodka.
5.  	Odpre se pogovorno okno.
6.  	Uredi polja za ime dogodka, datum in pomembnost.
7.  	Nato klikne na gumb za shranitev.
8.  	Dogodku se shranijo novi podatki.
9.  	Uporabnik je preusmerjen nazaj na koledar. 
 
#### Alternativni tok:
**Alternativni tok 1:**
1.  	Uporabnik je vpisan  v sistem.
2.  	Na domači strani izbere koledar, na katerem je dogodek, ki ga želi urediti.
3.  	Izbere dogodek. 
4.  	Odpre se pogovorno okno za urejanje dogodka. 
5.  	Na dnu pogovornega okna za urejanje je opcija za izbris dogodka.
6.  	Izbere iz katerih koledarjev želi izbrisati dogodek.
7.  	Nato klikne na gumb za shranitev.
8.	  Pokaže se pogovorno okno za potrditev izbrisa.
9. 	 Klikne »Da«.
10. 	 Dogodek se izbriše iz izbranih koledarjev.
11.  	Uporabnik je preusmerjen na domačo stran.
 
#### Izjemni tokovi:
-          Uporabnik uredi dogodek tako, da ima isto ime, datum in prioriteto in oznake kot že nek drug obstoječ dogodek. Sistem izpiše ustrezno obvestilo.
 
#### Pogoji
Uporabnik mora biti prijavljen v sistem in mora imeti ustvarjen vsaj en dogodek, da ga lahko ureja.
 
#### Posledice
Imamo spremenjene podatke dogodka. Če je uporabnik spremenil ime dogodka, novo ime dogodka postane zasedeno, staro ime pa se sprosti.
 
#### Prioritete identificiranih funkcionalnosti
**Must have.** Da so uporabniku ustvarjeni koledarji koristni, mora imeti možnost spreminjanja dogodkov v njih.
 
 
 
#### Sprejemni testi
-          Uporabnik spremeni ime dogodka
-	  Uporabnik izbriše dogodek
-          Uporabnik spremeni dogodek, da ima isto ime, datum in pomembnost kot že nek drug obstoječ dogodek.

 
###**VNAŠANJE OCEN**
 
#### Povzetek funkcionalnosti
Registrirani uporabnik lahko vnaša ocene. Pri tem mora določiti vrednost, datum pridobitve in predmet, pri katerem je pridobil oceno.
 
#### Osnovni tok: Registrirani uporabnik lahko vnaša ocene.
1.  	Uporabnik je vpisan v sistem.
2.  	Na domači strani izbere dodajanje ocen.
3.  	Odpre se pogovorno okno.
4.  	Izpolni vnosna polja za datum, predmet in oceno, ki jo je dobil.
5.  	Pritisne gumb shrani.
6.  	Podatki se shranijo.
7.  	Uporabnik je preusmerjen na domačo stran.
 
#### Izjemni tokovi:
-          Uporabnik vnese vrednost ocene, ki je manjša od 1 ali večja od 10. Sistem izpiše ustrezno obvestilo.
-	Uporabnik ne vnese predmeta. Sistem izpiše ustrezno obvestilo.
-	Uporabnik ne izbere datuma ocene. Sistem izpiše ustrezno besedilo.
 
#### Pogoji
Uporabnik mora biti prijavljen v sistem.
 
#### Posledice
Uporabnik ima vnešeno novo oceno.
 
#### Prioritete identificiranih funkcionalnosti
**Should have.**
 
#### Sprejemni testi
-          Uporabnik v redovalnico doda oceno, ki ji določi vrednost, datum in predmet.
-          Uporabnik poda oceno z vrednostjo 11.
-	Uporabnik poda datum in vrednost ocene, ne poda imena predmeta. Shrani oceno.
-	Uporabnik poda vrednost ocene in predmet, ne poda datuma ocene. Shrani oceno. 
 
###**UREJANJE OCEN**
 
#### Povzetek funkcionalnosti
Registrirani uporabnik lahko ureja ocene, ki jih je vnesel.
 
#### Osnovni tok:
1.  	Uporabnik je vpisan v sistem.
2.  	Na domači strani izbere redovalnico.
3.  	Odprejo se ocene.
4.  	Klikne na oceno.
5.  	Odpre se pogovorno okno z podatki.
6.  	Izpolni vnosna polja za datum, predmet in oceno, ki jo je dobil.
7.  	Pritisne gumb shrani.
8.  	Podatki se shranijo.
9.  	Uporabnik je preusmerjen na domačo stran.
 
#### Izjemni tokovi:
-          Uporabnik vnese vrednost ocene, ki je manjša od 1 ali večja od 10. Sistem izpiše ustrezno obvestilo.
-	Uporabnik ne vnese predmeta. Sistem izpiše ustrezno obvestilo.
 
#### Pogoji
Uporabnik mora biti prijavljen v sistem in mora imeti ustvarjeno vsaj eno oceno.
 
#### Posledice
Ocena je spremenjena.
 
#### Prioritete identificiranih funkcionalnosti
**Should have.**
 
#### Sprejemni testi
-       Uporabnik spremeni datum ocene.
-	Uporabnik vnese vrednost, ki je nižja od 1 ali višja od 10.
-	Uporabnik pobriše polje s predmetom in ga pusti praznega. Poskusi shraniti oceno.
 


 
## 6. Nefunkcionalne zahteve
1.       Sistem mora biti dosegljiv na javno dostopnem spletnem naslovu.
2.       Sistem ne sme omogočiti uporabnikom dostopa do njemu nepooblaščenih podatkov.
3.       Sistem mora biti na voljo najmanj 99.98% časa.
4.       Sistem mora podpirati 3000 hkratnih uporabnikov (z možnostjo kasnejše nadgradnje).
5.       Sistem mora podpirati 100000 registriranih uporabnikov z možnostjo kasnejše nadgradnje.
6.       Sistem mora omogočati potrditev registracije preko elektronskega naslova.
7.       Sistem mora imeti 3 ločene strežnike, ki so sinhronizirani.
8.       Sistem mora uporabniku omogočati izključitev piškotkov.
9.       Brskalnik, na katerem se bo uporabljalo aplikacijo, mora podpirati HTML5.
10.   Dostop do aplikacije mora biti omogočen samo preko https.
11.   Sistem mora omogočati »request desktop site«.
12.   Sistem mora imeti odzivnost na zahteve pod 5 sekund.
13.   Po 3 napačnih poskusih prijave sistem zahteva potrditev captcha.
14.   Vsi podatki, razen vsebin beležk, morajo biti hranjeni v .csv.
15.   Uporabljati je potrebno relacijsko podatkovno bazo.
16.   Uporabljati je potrebno brezplačnen API za zemljevide.
 
 
## 7. Prototipi vmesnikov
 

![Registracija](../img/zm/registracija1-2.jpg)
![Registracija](../img/zm/registracija2-1.jpg)

*Zaslonska slike postopka registracije*

![Prijava](../img/zm/Login-1.jpg)

*Zaslonska slika prijave v sistem*

![Odjava](../img/zm/prijavljen_osnovna_stran-1.jpg)

*Zaslonska slika za primerom odjave*

![Domaca stran](../img/zm/prijavljen_osnovna_stran-1.jpg)
![Domaca stran dodaj](../img/zm/drop_down_add_fixed-1.jpg)

*Zaslonska slika domače strani in menija za dodajanje vsebin*

![Spustni meni uporabnik](../img/zm/drop_down_user-1.jpg)
![Spustni meni administrator](../img/zm/drop_down_admin-1.jpg)

*Zaslonska slika spustnega menija za uporabnika in administratorja*

![Spremeni podatke](../img/zm/sprememba_podatkov-1.jpg)

*Zaslonska slika vmesnika za spreminjanje podatkov*

![Status prosnja](../img/zm/prošnja_za_status-1.jpg)

*Zaslonska slika vmesnika za oddajo prošnje za povišanje statusa*

![Status odobritev](../img/zm/potrditev_statusa-1.jpg)

*Zaslonska slika vmesnika za sprejemanje in zavračanje prošenj za povišanje statusa*

![Ocene](../img/zm/dodajanje_ocen_in_pregled_fix-1.jpg)

*Vmesnik za pregledovanje ocen*

![Ocene izvoz](../img/zm/ocene_in_izvoz-2.jpg)

*Vmesnik za izvoz ocen*

![Pregled urnika](../img/zm/kreacija_in_pregled_urnika-1.jpg)
![Pregled urnika preko URL](../img/zm/logged_id_other_urnik-1.jpg)
![Pregled urnika preko URL neprijavljen](../img/zm/anon_koledar-1.jpg)

*Zaslonske slike vmesnika za pregled lastnega urnika, urnika druge osebe ko smo prijavljeni in pregled urnika v anonimnem načinu*

![Pregled koledarja](../img/zm/lastnik_ustvari_preglej_koledar-1.jpg)
![Pregled koledarja preko URL](../img/zm/pregled_koledar_preko_URL-1.jpg)
![Pregled koledarja preko URL neprijavljen](../img/zm/pregled_koledar_preko_URL-2.jpg)

*Zaslonske slike vmesnika za pregled lastnega koledarja, koledarja druge osebe ko smo prijavljeni in pregled koledarja v anonimnem načinu*

![Belezka ustvari](../img/zm/beležka_vse-1.jpg)
![Belezka](../img/zm/beležka_vse-2.jpg)

*Primer zaslonski sliki ustvarjanja, urejanja in pregleda beležke*

![Plan dela priprava](../img/zm/generiranje_plana-1.jpg)
![Plan dela izvedba](../img/zm/generiranje_plana-2.jpg)

*Primer vmesnika za avtomatsko generiranje načrta dela*

![Lokacija dodaj](../img/zm/zemljevid_dogodek-1.jpg)

*Vmesnik za dodajanje lokacije k dogodku na koledarju*

![Urnik nov](../img/zm/kreacija_in_pregled_urnika-2.jpg)

*Vmesnik za kreiranje novega urnika*

![Urnik uredi](../img/zm/drop_down_urnik-2.jpg)
![Urnik dodaj in uredi vnos](../img/zm/add_update_koledar-1.jpg)

*Zaslonska slika spustnega menija za urejanje urnika in ustvarjalec novega ter urejevalnik starega vnosa v urnik*

![Koledar nov](../img/zm/lastnik_ustvari_preglej_koledar-2.jpg)

*Vmesnik ustvarjanja novega koledarja*

![Koledar nov dogodek](../img/zm/dodaj_dogodek-1.jpg)
![Koledar uredi dogodek](../img/zm/uredi_dogodek_koledar-1.jpg)
![Koledar izbriši vnos](../img/zm/izbris_dogodek_koledar-1.jpg)

*Vmesniki za ustvarjanje, urejanje in izbris dogodkov v koledarju*

![Ocena dodaj](../img/zm/dodajanje_ocen_in_pregled_fix-2.jpg)
![Ocena uredi](../img/zm/edit_ocena-1.jpg)

*Vmesnik za dodajanje, urejanje ocen*



## Vmesniki do zunanjih sistemov

#### Izobraževalni zavod: 
Zavod ponuja funkcijo preveri(podatkiOsebe). Prejme datoteko podatkiOsebe, v kateri so zapisani podatki, potrebni za overitev delovnega mesta osebe - ime, priimek in naziv izobraževalne ustanove, na kateri registrirani uporabnik, ki je oddal prošnjo, poučuje. Zavod vrne enega od treh možnih odgovorov: 
1.   "oseba je učitelj na šoli, navedeni v datoteki podatkiOsebe". Vsi podatki se ujemajo: oseba je podala resnične podatke. 
2.    "oseba ni učitelj na šoli, navedeni v datoteki podatkiOsebe". Oseba je zaposlena na navedeni šoli, ampak ni učitelj. 
3.   "oseba ni zaposlena na šoli". To pomeni, da oseba ni zaposlena na šoli, ali pa da se podatki ne ujemajo (oseba se je mogoče zatipkala pri pisanju katerega od podatkov). 

#### Najdi.si: 
Najdi.si ponuja funkciji prikažiZemljevid(lokacija) in pinLokacije(lokacija). V obeh funkcijah prejme lokacijo uporabnika in mu izriše zemljevid okoliša v radiju 5km. Funkcija pinLokacije omogoča, da uporabnik klikne na zemljevid za lociranje lokacije, ki se potem shrani.


