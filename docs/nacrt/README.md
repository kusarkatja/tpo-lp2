# Načrt sistema

| | |
|:---|:---|
| **Naziv projekta** | StraightAs |
| **Člani projektne skupine** | Andreja Jerič, Blaž Prosenc, Regina Polc, Katja Kušar |
| **Kraj in datum** | Ljubljana, 22.4.2019 |



## Povzetek

V dokumentu bomo predstavili načrt rešitve za sistem v projektu StraightAs. Vsebuje načrt arhitekture, načrt strukture in načrt obnašanja. Načrt strukture je prikazan z razrednim diagramom in opisi vseh elementov le-tega, načrt obnašanja pa vsebuje diagrame zaporedja, končne avtomate, diagrame aktivnosti, diagrame stanj in psevdokodo. Za prikaz načrta arhitekture smo uporabili diagrame za prikaz logičnega in razvojnega pogleda arhitekture.


## 1. Načrt arhitekture

Za predstavitev razvojnega pogleda je bila izbrana večplastna arhitektura. Ta sistem organizira v plasti s sorodno funkcionalnostjo. Sloj zagotavlja storitve sloju nad njim, tako da sloji najnižje ravni predstavljajo osnovne storitve, ki se bodo uporabljale v celotnem sistemu.

![<MVC>](../img/tpo-mvc.png)

Za predstavitev logičnega pogleda je bila izbrana arhitektura MVC. Komponenta model (M) upravlja sistemske podatke in povezane operacije na teh podatkih. Komponenta pogled (V) opredeljuje in upravlja z načinom predstavitve podatkov uporabniku. Komponenta krmilnik (C) upravlja uporabniško interakcijo (npr. pritiski tipk, kliki miške itd.) in te interakcije prenese v pogled in model, kot je prikazano na spodnji sliki.

![mvc](../img/MVC.png)


## 2. Načrt strukture

### 2.1 Razredni diagram

![razredniDiagram](../img/razredniDiagram2.jpg)

### 2.2 Opis razredov

Razredi predstavljajo splošno opredelitev določene vrste sistemskega objekta, asociacije med njimi pa obstoj razmerja med temi razredi.

V našem sistemu je vsega skupaj 14 razredov, od tega sta dva mejna razreda.

## Razred NEREGISTRIRANI UPORABNIK
 
Objekt razreda Neregistrirani uporabnik predstavlja uporabnika, ki ni registriran v sistemu.
 
### Atributi
 
Brez atributov.
 
### Nesamoumevne metode
 
1) **void registracija(String mail, String uporabniskoIme, String geslo, String ime, String priimek, int letnikSolanja, String smerSolanja, String nazivSole)**
	
-ustvari se nov objekt razreda Registrirani uporabnik, ki ima vrednosti atributov elektronskiNaslov “mail”, uporabniskoIme “uporabniskoIme”, geslo “geslo”, ime “ime”, priimek “priimek”, letnikSolanja “letnikSolanja”, smerSolanja “smerSolanja”, nazivSole “nazivSole” in visjiStatus “false”, če so vsi podatki veljavni (elektronski naslov “mail” še ni registriran, uporabniško ime “uporabniskoIme” še ni registrirano, ostali podatki so skladni - upoštevajo predpisano zalogo vrednosti). Na elektronski naslov “mail” se pošlje potrditveni link. Če link ni potrjen v enem dnevu, se ta nov objekt izbriše.

2) **Urnik pregledUrnikaURL(String url)**

-vrne urnik nekega registriranega uporabnika, če je url veljaven.
 
3) **Koledar pregledKoledarjaURL(String url)**

-vrne koledar nekega registriranega uporabnika, če je url veljaven
 
## Razred VIŠJI REGISTRIRANI UPORABNIK
 
Objekt razreda Višji registrirani uporabnik predstavlja višjega registriranega uporabnika. Deduje vse metode razreda Neregistrirani uporabnik.
 
### Atributi
 
1) **uporabniskoIme: String**

2) **geslo: String**

-zaloga vrednosti: vsaj šest znakov, od tega vsaj ena številka in vsaj ena velika črka

3) **elektronskiNaslov: String**

4) **visjiStatus:  boolean**

-če je nastavljen na true, pomeni, da ima oseba višji status - je višji registrirani uporabnik

5) **ime: String**

-začeti se mora z veliko začetnico, mora vsebovati samo črke

6) **priimek: String**

-začeti se mora z veliko začetnico, mora vsebovati samo črke

7) **letnikSolanja: int**

-zaloga vrednosti: 1-9

8) **smerSolanja: String**

-zaloga vrednosti: samo črke

9) **nazivSole: String**

-zaloga vrednosti: samo črke
 
### Nesamoumevne metode
 
1) **void prijava(String prvo, String geslo)**

-String prvo predstavlja ali uporabniško ime ali elektronski naslov, ki ga uporabnik vnese. Prijavi registriranega uporabnika v sistem, če je kombinacija uporabniškega imena ali elektronskega naslova “prvo” in gesla “geslo” veljavna.

2) **void odjava()**

-uporabnika, ki je prijavljen v sistemu, odjavi iz sistema.

3) **Urnik pregledUrnika(Urnik urnik)**

-vrne objekt razreda Urnik “urnik”. To vključuje tudi vse elemente, ki so na tem urniku.

4) **void ustvarjanjeUrnika(String ime, boolean vidljivost)**

-ustvari nov, prazen (=brez elementov) objekt razreda Urnik, ki pridobi vrednost atributov ime “ime” in vidljivost “vidljivost”

5) **void urejanjeUrnika(Urnik urnik, String ime, boolean vidljivost)**

-spremeni objektu razreda Urnik “urnik” vrednosti atributov ime na “ime” in vidljivost na “vidljivost”

6) **Koledar pregledKoledarja(Koledar koledar)**

-vrne objekt razreda Koledar “koledar”. To vključuje tudi vse dogodke, ki so na tem koledarju.

7) **void ustvarjanjeKoledarja(String ime, boolean vidljivost)**

-ustvari nov, prazen (=brez dogodkov) objekt razreda Koledar, ki pridobi vrednost atributov ime “ime” in vidljivost “vidljivost”

8) **void ustvarjanjeBelezke(String ime)**

-ustvari nov, prazen (=vrednost atributa vsebina je NULL) objekt razreda Belezka, ki pridobi vrednost atributa ime “ime”

9) **void urejanjeBelezke(Beležka belezka, String ime, String besedilo)**

-objektu razreda Beležka “belezka” spremeni vrednosti atributov ime na “ime” in vsebina na “besedilo”

10) **void ustvarjanjeDogodka(Koledar koledar, String ime, Date datum, int prioriteta)**

-ustvari nov objekt razreda Dogodek v objektu razreda Koledar “koledar”, ki pridobi ime “ime”, datum “datum” in prioriteto “prioriteta”

11) **void urejanjeDogodkov(Dogodek dogodek, String ime, Date datum, int prioriteta)**

-objektu razreda Dogodek “dogodek” spremeni vrednosti atributov ime na “ime”, datum na “datum” in prioriteta na “prioriteta”

12) **void ustvarjanjeElementov(Urnik urnik, String predmet, String dan, String zacetek, int trajanje)**

-ustvari nov objekt razreda Element v objektu razreda Urnik “urnik”, ki pridobi ime “predmet”, dan “dan”, začetek “zacetek” in trajanje “trajanje”.

13) **void urejanjeElementov(Element element, String ime, String dan, String zacetek, int trajanje)**

-objektu razreda Element “element” spremeni vrednost atributov ime na “ime”, dan na “dan”, zacetek na “zacetek” in trajanje na “trajanje”

14) **void Izvoz(Ocena [] ocene)**

-ustvari datoteko, v kateri se nahaja seznam objektov razreda Ocena “ocene”

15) **Beležka pregledBelezke(Beležka belezka)**

-vrne objekt razreda Beležka “belezka”.

16) **void izbrisBelezke(Beležka belezka)**

-izbriše objekt razreda Beležka “belezka”.

17) **void izbrisDogodka(Dogodek dogodek)**

-izbriše objekt razreda Dogodek “dogodek”.

18) **void izbrisElementa(Element element)**

-izbriše objekt razreda Element “element”

19) **void ustvariNacrt(Dogodek [] dogodki)**

-ustvari nov objekt razreda Načrt dela, ki zajame objekte razreda Dogodek v seznamu “dogodki” in ustrezno dodeli vrednost atributu besediloNacrta.

20)	**void vnosOcene(int vrednost, Date datum, String predmet)**

-ustvari nov objekt razreda Ocena, ki nastavi vrednosti atributov vrednost na “vrednost”, datum na “datum” in predmet na “predmet”

21)	**void urejanjeOcene(Ocena ocena, int vrednost, Date datum, String predmet)**

-objektu razreda Ocena “ocena” nastavi vrednosti atributov vrednost na “vrednost”, datum na “datum” in predmet na “predmet”

22)	**Ocena [] pregledOcen(String predmet)**

-vrne seznam objektov razreda Ocena, ki imajo vrednost atributa predmet nastavljeno na “predmet”. Če je vrednost “predmeta” NULL, vrne vse objekte razreda Ocena.

23)	**void urediPodatke(String username, String geslo, String ime, String priimek, int letnikSolanja, String smerSolanja, String nazivSole)**

-spremeni vrednosti atributov uporabnika uporabniskoIme na “uporabniskoIme”, geslo na “geslo”, ime na “ime”, priimek na “priimek”, letnikSolanja na “letnikSolanja”, smerSolanja na “smerSolanja”, nazivSole na “nazivSole”, če so vsi podatki veljavni.

24)	**void pinDogodka(Dogodek dogodek)**

-uporabita se metodi, ki jih ponuja najdi.si: prikaziZemljevid() in pinLokacije(). Niz, ki ga vrne metoda pinLokacije, se nastavi kot vrednost atributa lokacija objekta razreda Dogodek “dogodek”

## Razred REGISTRIRANI UPORABNIK
 
Objekt razreda Registrirani uporabnik predstavlja registriranega uporabnika. Deduje vse atribute in metode razreda Višji registrirani uporabnik. Dodana je še ena metoda, ki pripada samo njemu – oddajaVloge().

### Atributi
 
Brez dodatnih atributov.
 
### Nesamoumevne metode
 
1) **void oddajaVloge(String nazivSole)**

-ustvari objekt razreda Vloga. Ta vsebuje uporabnikovo ime, priimek in uporabniško ime (ki deluje kot identifikacijska številka vloge) ter poln naziv šole “nazivSole”, na kateri naj bi uporabnik bil profesor. Vloga se posreduje administratorju.
 
## Razred ADMINISTRATOR
 
Objekt razreda Administrator predstavlja administratorja, ki upravlja z vlogami za povišanje statusa in dodeljuje višji status registriranim uporabnikom.

 
### Atributi

1)	**ID : int**

-unikatna identifikacijska številka

2)	**ime : String**

-začeti se mora z veliko začetnico, mora vsebovati samo črke

3)	**priimek : Sting**

-začeti se mora z veliko začetnico, mora vsebovati samo črke

4)	**geslo : String**

-zaloga vrednosti: vsaj šest znakov, od tega vsaj ena številka in vsaj ena velika črka

5)	**elektronskiNaslov : String**
 
### Nesamoumevne metode
 
1) **void prijava(int ID, String geslo)**

-prijavi administratorja v sistem, če je kombinacija identifikacijske številke “ID” in gesla “geslo” veljavna.

2) **Vloga preglejVlogo(Vloga vloga)**

-vrne vse podatke, ki so v objektu razreda Vloga “vloga”

3) **void zakljuciVlogo(Vloga vloga, boolean zakljucitev)**

-uporabniku, ki ima isto uporabniško ime, kot je ID objekta razreda Vloga “vloga”, se dodeli višji status, če je vrednost “zakljucitev” true: uporabniku se vrednost atributa visjiStatus spremeni na “true”. Sicer uporabnik ostane registrirani uporabnik. Potem se objekt “vloga” izbriše. 

4) **boolean overiZaposlitev(Vloga vloga)**

-uporabi se funkcija izobraževalnega zavoda preveri(File oseba). V datoteko “oseba” se shrani vrednosti atributov ime, priimek in nazivSole vloge “vloga”. Če funkcija preveri vrne “Oseba je učitelj na šoli, navedeni v datoteki podatkiOsebe", funkcija overiZaposlitev() vrne true, sicer false.
 
 
## Razred VLOGA
 
Objekt razreda Vloga predstavlja vlogo za povišanje statusa, ki jo uporabniki lahko oddajo in administratorji lahko preglejujejo in zaključujejo.
 
### Atributi
 
1) **ime : String**

2) **priimek : String**

3) **nazivSole: String**

4) **ID: String**
 
### Nesamoumevne metode
 
Brez nesamoumevnih metod.
 
 
## Razred NAČRT DELA
 
Objekt razreda Načrt predstavlja načrt dela, ki ga sistem lahko zgenerira registriranemu uporabniku.
 
### Atributi
 
1)	**besediloNacrta: String**

-v vsaki vrstici je zapisan datum, poleg njega pa aktivnosti, ki jih mora uporabnik izvesti tisti dan, ki so kar imenadogodkov, ki jih je uporabnik izbral. Pogostotst posameznih dogodkov in je odvisna od njihovih prioritet.
 
### Nesamoumevne metode
 
Brez nesamoumevnih metod. 
 
## Razred OCENA
 
Objekt razreda Ocena predstavlja oceno registriranega uporabnika. Ta lahko ocene vnaša, ureja in briše.
 
### Atributi
 
1) **vrednost: int**

-zaloga vrednosti: 1-10

2) **datum: Date**

3) **predmet: String**
 
### Nesamoumevne metode
 
Brez nesamoumevnih metod.

 
## Razred BELEŽKA
 
Objekt razreda Beležka predstavlja beležko registriranega uporabnika. Ta lahko beležke ustvarja, briše in ureja.
 
### Atributi
 
1) **ime : String**

2) **vsebina: String**
 
### Nesamoumevne metode
 
Brez nesamoumevnih metod.
 
 
## Razred DOGODEK
 
Objekt razreda Dogodek predstavlja dogodek v koledarju, ki jih registrirani uporabniki lahko ustvarjajo, brišejo in urejajo.
 
### Atributi
 
1) **ime: String**

2) **datum: Date**

3) **prioriteta: int**

-zaloga vrednosti: 1-10

4) **koledar: Koledar**

5) **lokacija: String**
 
### Nesamoumevne metode
 
Brez nesamoumevnih metod.
 
 
## Razred KOLEDAR
 
Objekt razreda Koledar predstavlja koledar, ki ga lahko uporabniki ustvarjajo, urejajo in vanj vnašajo dogodke.
 
### Atributi
 
1) **ime: String**

2) **vidljivost: boolean**

3) **dogodki: Dogodek []**
 
### Nesamoumevne metode
 
Brez nesamoumevnih metod. 
 
## Razred ELEMENT
 
Objekt razreda Element predstavlja element na urniku. Te lahko registrirani uporabniki ustvarjajo, brišejo in urejajo.
 
### Atributi
 
1) **predmet: String**

2) **urnik: Urnik**

3) **dan: String**

-zaloga vrednosti: ponedeljek, torek, sreda, četrtek, petek, sobota, nedelja

4) **zacetek: String**

-zaloga vrednosti: X:YZ, kjer 0<=X<=23 in 0<=Y<=5 in 0<=Z<=9

5) **trajanje: int**

-zaloga vrednosti: od nič naprej
 
### Nesamoumevne metode
 
Brez nesamoumevnih metod.
 
 
## Razred URNIK
 
Objekt razreda Urnik predstavlja urnik, ki vsebuje elemente in ga registrirani uporabniki lahko ustvarjajo, brišejo in urejajo.
 
### Atributi
 
1) **ime: String**

2) **vidljivost: boolean**

3) **elementi: Element[]**
 
### Nesamoumevne metode
 
Brez nesamoumevnih metod.


## Razred SISTEM ZAVODA
 
Objekt razreda Sistem zavoda je mejni razred. Vsebuje funkcije, ki jih izobraževalni zavod ponuja administratorju našega sistema.
 
### Atributi
 
Brez atributov.
 
### Nesamoumevne metode
 
1) **String preveri(File podatkiOsebe)**

-prejme datoteko podatkiOsebe, ki jo je administrator posredoval. Vrne enega od treh možnih odgovorov: 

    1. "Oseba je učitelj na šoli, navedeni v datoteki podatkiOsebe". Vsi podatki v datoteki se ujemajo: oseba je podala resnične podatke in je zaposlena kot profesor. 

    2. "Oseba ni učitelj na šoli, navedeni v datoteki podatkiOsebe". Vsi podatki v datoteki se ujemajo: oseba je podala resnične podatke, ampak ni zaposlena kot profesor. 

    3. "Oseba ni zaposlena na šoli". To pomeni, da oseba ni zaposlena na šoli, ali pa se podatki ne ujemajo.

## Razred NAJDI.SI
 
Objekt razreda Najdi.si je mejni razred, ki predstavlja API od najdi.si. Vsebuje funkcije, ki jih najdi.si ponuja uporabniku našega sistema.
 
### Atributi
 
Brez atributov.
 
### Nesamoumevne metode
 
1) **Zemljevid prikaziZemljevid()**

-prejme lokacijo uporabnika v obliki naslova, če brskalnik uporabnika to omogoča, sicer se uporabi lokacija Mestni trg 25, 1000 Ljubljana. Izriše zemljevid okoliša v radiju 5km lokacije. Uporabnik lahko premika zemljevid, da vidi tudi drugo okolico.

2) **String pinLokacije()**

-prejme lokacijo, na katero je uporabnik na zemljevidu kliknil. Lokacijo pretvori v naslov, ki ji je najbližje, in to kot niz znakov vrne uporabniku.


## 3. Načrt obnašanja



#### Diagrami zaporedja

![<Dodajanje elementov v urnik>](../img/Sequence/Dodajanje_elementov_urnika.png)
![<Dodajanje dogodkov v koledar>](../img/Sequence/Dodajanje_dogodka_koledarja.png)
![<Generiranje načrta>](../img/Sequence/Avtomatsko_generiranje_načrta_dela.png)
![<Izvoz ocen>](../img/Sequence/Izvoz_ocen.png)
![<Lociranje dogodkov>](../img/Sequence/Lociranje_dogodkov.png)
![<Oddaja vloge>](../img/Sequence/Oddaja_vloge.png)
![<Odjava>](../img/Sequence/Odjava.png)
![<Pregled koledarja z URL-jem>](../img/Sequence/Pregled_koledarjaURL.png)
![<Pregled lastnega koledarja>](../img/Sequence/Pregled_lastnega_koledarja.png)
![<Pregled lastnega urnika>](../img/Sequence/Pregled_lastnega_urnika.png)
![<Pregled urnika z URL-jem>](../img/Sequence/Pregled_urnikaURL.png)
![<Prijava>](../img/Sequence/Prijava.png)
![<Registracija>](../img/Sequence/Registracija.png)
![<Urejanje belezke>](../img/Sequence/Urejanje_beležk.png)
![<Urejanje dogodkov v koledarju>](../img/Sequence/Urejanje_dogodkov.png)
![<Urejanje elementov v urnik>](../img/Sequence/Urejanje_elementov.png)
![<Urejanje ocen>](../img/Sequence/Urejanje_ocen.png)
![<Urejanje osebnih podatkov>](../img/Sequence/Urejanje_osebnih_podatkov.png)
![<Ustvarjanje beležk>](../img/Sequence/Ustvarjanje_beležk.png)
![<Ustvarjanje koledarja>](../img/Sequence/Ustvarjanje_koledarja.png)
![<Ustvarjanje urnika>](../img/Sequence/Ustvarjanje_urnika.png)
![<Vnos ocen>](../img/Sequence/Vnašanje_ocen.png)
![<Zaključevanje statusa>](../img/Sequence/zaključevanje_vlog.png)



#### Diagrami aktivnosti

![<Dodajanje elementov v urnik>](../img/Activity/Screenshot_1.jpg)
![<Generiranje načrta>](../img/Activity/Screenshot_2.jpg)
![<Izvoz ocen>](../img/Activity/Screenshot_3.jpg)
![<Lociranje dogodkov>](../img/Activity/Screenshot_4.jpg)
![<Oddaja vloge>](../img/Activity/Screenshot_5.jpg)
![<Odjava>](../img/Activity/Screenshot_6.jpg)
![<Pregled koledarja z URL-jem>](../img/Activity/Screenshot_7.jpg)
![<Pregled lastnega koledarja>](../img/Activity/Screenshot_8.jpg)
![<Pregled lastnega urnika>](../img/Activity/Screenshot_9.jpg)
![<Pregled urnika z URL-jem>](../img/Activity/Screenshot_10.jpg)
![<Prijava>](../img/Activity/Screenshot_11.jpg)
![<Registracija>](../img/Activity/Screenshot_12.jpg)
![<Urejanje belezke>](../img/Activity/Screenshot_13.jpg)
![<Urejanje dogodkov v koledarju>](../img/Activity/Screenshot_14.jpg)
![<Urejanje elementov v urnik>](../img/Activity/Screenshot_15.jpg)
![<Urejanje ocen>](../img/Activity/Screenshot_16.jpg)
![<Urejanje osebnih podatkov>](../img/Activity/Screenshot_17.jpg)
![<Ustvarjanje beležk>](../img/Activity/Screenshot_18.jpg)
![<Ustvarjanje koledarja>](../img/Activity/Screenshot_19.jpg)
![<Ustvarjanje urnika>](../img/Activity/Screenshot_20.jpg)
![<Vnos ocen>](../img/Activity/Screenshot_21.jpg)
![<Zaključevanje statusa>](../img/Activity/Screenshot_22.jpg)
![<Dodajanje dogodkov v koledar>](../img/Activity/Screenshot_40.jpg)


### Končni avtomat
![avtomat](../img/alfa_koncni_s.png)

[Bolj natančen pogled je na voljo na tem linku](../img/alfa_koncni.png)


#### Diagrami stanj

![<Dodajanje elementov v urnik>](../img/stanja/Dodajanje_elementa.png)
![<Dodajanje dogodkov v koledar>](../img/stanja/Dodajanje_dogodka.png)
![<Generiranje načrta>](../img/stanja/Avtomatsko_generiranje.png)
![<Izvoz ocen>](../img/stanja/Izvoz_ocen.png)
![<Lociranje dogodkov>](../img/stanja/Lociranje_dogodkov.png)
![<Oddaja vloge>](../img/stanja/Oddaja_vloge.png)
![<Odjava>](../img/stanja/Odjava.png)
![<Pregled koledarja z URL-jem>](../img/stanja/Pregled_koledarjaURL.png)
![<Pregled lastnega koledarja>](../img/stanja/Pregled_lastnega_koledarja.png)
![<Pregled lastnega urnika>](../img/stanja/Pregled_lastnega_urnika.png)
![<Pregled urnika z URL-jem>](../img/stanja/Pregled_urnikaURL.png)
![<Prijava>](../img/stanja/Prijava.png)
![<Registracija>](../img/stanja/Registracija.png)
![<Urejanje belezke>](../img/stanja/Urejanje_beležke.png)
![<Urejanje dogodkov v koledarju>](../img/stanja/Urejanje_dogodka.png)
![<Urejanje elementov v urnik>](../img/stanja/Urejanje_elementa.png)
![<Urejanje ocen>](../img/stanja/Urejanje_ocene.png)
![<Urejanje osebnih podatkov>](../img/stanja/Urejanje_osebnih_podatkov.png)
![<Ustvarjanje beležk>](../img/stanja/Ustvarjanje_beležke.png)
![<Ustvarjanje koledarja>](../img/stanja/Ustvarjanje_koledarja.png)
![<Ustvarjanje urnika>](../img/stanja/Ustvarjanje_urnika.png)
![<Vnos ocen>](../img/stanja/Dodajanje_ocene.png)
![<Zaključevanje statusa>](../img/stanja/Zaključevanje_vlog.png)


